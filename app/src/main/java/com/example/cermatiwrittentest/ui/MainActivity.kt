package com.example.cermatiwrittentest.ui

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.cermatiwrittentest.R
import com.example.cermatiwrittentest.model.GithubUserData
import com.example.cermatiwrittentest.repository.MainRepository
import com.example.cermatiwrittentest.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.search_layout.*
import kotlinx.coroutines.ExperimentalCoroutinesApi


@ExperimentalCoroutinesApi
class MainActivity : BaseActivity() {

    private lateinit var mViewModel: MainViewModel
    private lateinit var mAdapter: MainAdapter
    private lateinit var mListData: ArrayList<GithubUserData>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initObject()
        initObserver()
        initAdapter()
        initListener()
    }

    private fun initObject() {
        mListData = ArrayList()
        mAdapter = MainAdapter()
        mViewModel = ViewModelProvider(
            this,
            MainViewModel.FACTORY(MainRepository())
        ).get(MainViewModel::class.java)
    }

    private fun initObserver() {
        mViewModel.userLiveData.observe(this) {
            Log.d("Activity", "list: ${it?.size}")
            it?.forEach { item -> mListData.add(item.copy()) }
            mAdapter.submitList(mListData) //Memakai DifUtils mengatasi masalah flickering pada view dan meningkatkan peforma
        }

        mViewModel.emptyList.observe(this) {
            emptyList.visibility = if (it && mListData.size == 0) View.VISIBLE else View.GONE
        }

        mViewModel.searchBarLoading.observe(this) {
            search_progress_bar.visibility = if (it) View.VISIBLE else View.GONE
        }

        mViewModel.loading.observe(this) {
            mAdapter.loading = it
        }

        defaultErrorMessageHandling(mViewModel.error)
    }

    private fun initAdapter() {
        recycle_list_user.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recycle_list_user.setHasFixedSize(true)
        recycle_list_user.adapter = mAdapter
    }

    private fun initListener() {
        search_edit_text.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                hideKeyboard()
                searchUserData()
            }
            true
        }

        recycle_list_user.addOnScrollListener(object :
            RecyclerView.OnScrollListener() { //scroll listener
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val layoutManager = recycle_list_user.layoutManager as LinearLayoutManager
                val totalItemCount = layoutManager.itemCount
                val visibleItemCount = layoutManager.childCount
                val firstVisibleItem = layoutManager.findFirstVisibleItemPosition()

                mViewModel.listScrolled(visibleItemCount, firstVisibleItem, totalItemCount)
            }
        })
    }

    private fun hideKeyboard() {
        search_edit_text.clearFocus()
        val inputManager: InputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(search_edit_text.windowToken, 0) //hide keyboard
    }

    private fun searchUserData() {
        performNetwork {
            search_edit_text.text.trim().let { username ->
                recycle_list_user.scrollToPosition(0)
                mViewModel.searchUserData(username.toString())
                mListData = ArrayList()
                mAdapter.submitList(null)
            }
        }
    }
}
