package com.example.cermatiwrittentest.ui

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.cermatiwrittentest.R
import com.example.cermatiwrittentest.model.GithubUserData
import com.squareup.picasso.Picasso

const val VIEW_TYPE_NORMAL = 0
const val VIEW_TYPE_LOADING = 1

class MainAdapter : ListAdapter<GithubUserData, RecyclerView.ViewHolder>(DATA_COMPARATOR) { //DifUtils -> Compare old data with new data

    var loading: Boolean = true
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return if (viewType == VIEW_TYPE_NORMAL) ViewHolder( //Item layout
            layoutInflater.inflate(
                R.layout.card_user_info,
                parent,
                false
            )
        )
        else LoadingViewHolder(layoutInflater.inflate(R.layout.card_loading, parent, false)) //Loading layout
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == VIEW_TYPE_NORMAL) {
            val listData = getItem(position)
            holder as ViewHolder
            Picasso.get()
                .load(listData?.avatarUrl)
                .placeholder(R.drawable.ic_person)
                .error(R.drawable.ic_person)
                .into(holder.imageUser)
            holder.nameUser.text = listData?.username // username
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageUser: ImageView = itemView.findViewById(R.id.image_user)
        val nameUser: TextView = itemView.findViewById(R.id.name_user)
    }

    inner class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun getItemViewType(position: Int): Int {
        return if (loading && position == itemCount - 1) {
            VIEW_TYPE_LOADING
        } else {
            VIEW_TYPE_NORMAL
        }
    }

    companion object {
        //DiffUtil uses Eugene W. Myers's difference algorithm to calculate the minimal number of updates to convert one list into another.
        private val DATA_COMPARATOR = object : DiffUtil.ItemCallback<GithubUserData>() {
            override fun areItemsTheSame(oldItem: GithubUserData, newItem: GithubUserData): Boolean =
                oldItem.username == newItem.username

            override fun areContentsTheSame(oldItem: GithubUserData, newItem: GithubUserData): Boolean =
                oldItem == newItem
        }
    }


}
