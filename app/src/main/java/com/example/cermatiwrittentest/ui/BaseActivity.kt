package com.example.cermatiwrittentest.ui

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.example.cermatiwrittentest.R
import com.example.cermatiwrittentest.util.checkInternetConnection
import com.example.cermatiwrittentest.util.showAlertDialog
import com.google.android.material.snackbar.Snackbar
import retrofit2.HttpException

/**
 * Created by Exel staderlin on 11/3/2018.
 */

abstract class BaseActivity : AppCompatActivity() {

    fun <T : Throwable> defaultErrorMessageHandling(liveData: MutableLiveData<T>, observer: Observer<T>) {
        liveData.observe(this, observer)
        checkHttpException(liveData)
    }

    fun <T : Throwable> defaultErrorMessageHandling(liveData: MutableLiveData<T>) {
        checkHttpException(liveData)
    }

    private fun <T : Throwable> checkHttpException(liveData: MutableLiveData<T>) {
        liveData.observe(this, Observer<T> {
            it?.printStackTrace()
            if (it is HttpException) {
                httpExceptionHandling(it)
            } else {
                Snackbar.make(findViewById(android.R.id.content), it.toString(), Snackbar.LENGTH_LONG).show()
            }
        })
    }

    fun performNetwork(onValid: () -> Unit) {
        when(checkInternetConnection(this)) {
            true -> onValid()
            else -> showAlertDialog(this, "Please turn on your network"){/* do nothing */}
        }
    }

    private fun httpExceptionHandling(e: HttpException) {
        when (e.code()) {
            400 -> showAlertDialog(this, getString(R.string.bad_request)){/* do nothing */}
            401 -> showAlertDialog(this, getString(R.string.session_expired)) {/* do nothing */}
            403 -> showAlertDialog(this, getString(R.string.error_access_right)){/* do nothing */}
            404 -> showAlertDialog(this, getString(R.string.error_page_doesnt_exist)){/* do nothing */}
            408 -> showAlertDialog(this, getString(R.string.error_rto)){/* do nothing */}
            410 -> showAlertDialog(this, getString(R.string.page_no_longer_available)) {/* do nothing */}
            422 -> showAlertDialog(this, getString(R.string.error_data_invalid)) {/* do nothing */}
            500 -> showAlertDialog(this, getString(R.string.error_internal_server)){/* do nothing */}
            else -> showAlertDialog(this, getString(R.string.error_submit_problem)){/* do nothing */}

        }
    }

}