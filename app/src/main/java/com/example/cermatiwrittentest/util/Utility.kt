package com.example.cermatiwrittentest.util


import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.example.cermatiwrittentest.R


/**
 * Created by Exel staderlin on 23/07/2016.
 */


    @SuppressLint("InflateParams")
    fun showAlertDialog(context: Activity, message: String, onValid: () -> Unit) {
        val dialog: AlertDialog
        val builder = AlertDialog.Builder(context)
        val inflater = context.layoutInflater
        val dialogView = inflater.inflate(R.layout.dialog_alert, null)

        val messages = dialogView.findViewById<TextView>(R.id.message)
        val ok = dialogView.findViewById<TextView>(R.id.positive_button)

        builder.setView(dialogView)
        messages.text = message
        dialog = builder.create()
        dialog.show()
        dialog.setCancelable(false)
        ok.setOnClickListener {
            dialog.dismiss()
            onValid()
        }
    }

    fun checkInternetConnection(context: Context): Boolean {
        val localNetwork =
            (context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).activeNetworkInfo
        return localNetwork != null
    }

