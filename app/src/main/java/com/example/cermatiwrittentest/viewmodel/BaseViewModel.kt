package com.example.cermatiwrittentest.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
abstract class BaseViewModel : ViewModel() {

    val searchBarLoading = MutableLiveData<Boolean>()

    val loading = MutableLiveData<Boolean>()

    val emptyList = MutableLiveData<Boolean>()

    val error = MutableLiveData<Throwable>()

    fun <T> launchDataLoad(flowEmitData: Flow<T>, block: suspend (data: T) -> Unit) {
        //viewModelScope Function (Default Dispatcher Main)
        viewModelScope.launch {
            flowEmitData
                .onStart {
                    loading.value = true
                }
                .catch { throwable ->
                    searchBarLoading.value = false
                    loading.value = false
                    error.value = throwable
                    Log.d("throwable : ", throwable.toString())
                }
                .collect { data -> //response yang sudah ready akan di collect dan ready untuk di kirim ke UI Thread
                    loading.value = false
                    block(data)
                }

        }
    }

}