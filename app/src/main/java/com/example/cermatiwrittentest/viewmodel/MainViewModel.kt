package com.example.cermatiwrittentest.viewmodel

import androidx.lifecycle.MutableLiveData
import com.example.cermatiwrittentest.model.GithubUserData
import com.example.cermatiwrittentest.repository.MainRepository
import com.example.cermatiwrittentest.util.singleArgViewModelFactory
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class MainViewModel(private var mainRepository: MainRepository) : BaseViewModel() {

    private var page: Int = 1
    private var lastQuery: String? = null

    val userLiveData = MutableLiveData<List<GithubUserData>>() //Triggered LiveData when data is ready

    fun searchUserData(username: String) { //button search event
        page = 1
        when {
            username.isNotEmpty() -> {
                lastQuery = username
                searchBarLoading.value = true
                emptyList.value = false
                getUserData()
            }
            else -> emptyList.value = true
        }

    }

    fun listScrolled(visibleItemCount: Int, firstVisibleItemPosition: Int, totalItemCount: Int) {
        if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
            && firstVisibleItemPosition >= 0
            && totalItemCount >= VISIBLE_THRESHOLD
        ) {
            if (lastQuery != null && !loading.value!!) { // avoid triggering multiple requests in the same time
                page++
                if (emptyList.value == false) //not search when list is empty
                    getUserData()
            }
        }
    }

    private fun getUserData() {
        val flowData = mainRepository.getUserData(lastQuery!!, page)
        launchDataLoad(flowData) { data -> //Perform network request and optimized by kotlin coroutines dispatcher
            emptyList.value = data.items?.isEmpty()
            searchBarLoading.value = false
            userLiveData.value = data.items
        }
    }

    companion object {
        val FACTORY = singleArgViewModelFactory(::MainViewModel) //factory dibutuhkan untuk parsing repo ke viewmodel
        private const val VISIBLE_THRESHOLD = 5
    }

}