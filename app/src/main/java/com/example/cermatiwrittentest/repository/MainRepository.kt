package com.example.cermatiwrittentest.repository

import com.example.cermatiwrittentest.api.RestClient
import com.example.cermatiwrittentest.model.GithubCallbackData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

@ExperimentalCoroutinesApi
class MainRepository {

    fun getUserData(
        username: String,
        page: Int
    ): Flow<GithubCallbackData> {
        return flow {
            val request = RestClient.getApiInterface()
                .searchUser(username, page, NETWORK_PAGE_SIZE)
            emit(request) //Emit request when response is coming and response ready to be collected
        }.flowOn(Dispatchers.IO) //Dispatcher IO run on background thread for network request
    }

    companion object {
        const val NETWORK_PAGE_SIZE = 10
    }
}