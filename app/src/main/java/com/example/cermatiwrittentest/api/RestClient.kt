package com.example.cermatiwrittentest.api

import android.util.Log
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RestClient {


    fun getApiInterface(): ApiInterface {
        val okHttpClient = buildOkHttpClient(TOKEN)
        val retrofit = createRetrofit(okHttpClient)
        return retrofit.create(ApiInterface::class.java)
    }

    private fun createRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .also { it.baseUrl(URL) }
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun buildOkHttpClient(token: String, testInterceptor: Interceptor? = null): OkHttpClient {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder()
            .addInterceptor(logging)
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .addInterceptor { chain ->
                val original = chain.request()
                val request: Request
                request = original
                    .newBuilder()
                    .header("Content-Type", "application/x-www-form-urlencoded")
                    .also {
                        if (token.isNotEmpty()) {
                            it.header("Authorization", token)
                        }
                        Log.d("Authorization", token)
                    }
                    .method(original.method, original.body)
                    .build()

                val response = chain.proceed(request)
                response
            }
            .also {
                if (testInterceptor != null) {
                    it.addInterceptor(testInterceptor)
                }
            }
            .build()
    }

}