package com.example.cermatiwrittentest.api

import com.example.cermatiwrittentest.model.GithubCallbackData
import com.example.cermatiwrittentest.model.GithubUserData
import retrofit2.http.*

/**
 * Created by Exel staderlin on 7/12/2017.
 */
interface ApiInterface {

    @GET("search/users")
    suspend fun searchUser(
        @Query("q") username: String,
        @Query("page") page: Int,
        @Query("per_page") itemsPerPage: Int
    ): GithubCallbackData
}
